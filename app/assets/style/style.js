import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  productPictureSec: {
    width: '50%', 
    height: '100%',
    borderRightWidth: 3, 
    borderStyle: 'dashed',
    borderRightColor: '#CFCFCF',
  },

  productFormSec: {
    width: '50%', 
    height: '100%',
  },

  productTitle: {
  	fontSize: 20,
  	marginTop: 20,
  	marginRight: 'auto',
  	marginLeft: 'auto',
  },

  inputStyle: {
    width: '100%',
  	borderBottomWidth: 2,
  	marginTop: 20, 
  	borderStyle: 'solid',
  	marginRight: 'auto',
  	marginLeft: 'auto',
  },

  inputStyleBottom: {
    width: '100%', 
    borderBottomWidth: 2,
    marginTop: 20, 
    marginBottom: 20, 
    borderStyle: 'solid',
    marginRight: 'auto',
    marginLeft: 'auto',
  },

  submitBtn01: {
    marginTop: 20,
    marginRight: 'auto',
    marginLeft: 'auto',
  },

  button: {
    margin: '30px',
    fontSize: 18,
    height: '65px',
    width: '250px',
    borderRadius: 10,
  },

  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    marginTop: 50,
  },

  formStyle: {
    height: 100,
    width: 300, 
    marginRight: 'auto',
    marginLeft: 'auto',
  },

});
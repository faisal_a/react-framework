import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Products from './products/products';
import Transaction from './transaction/transaction';

const MainNavigator = createMaterialBottomTabNavigator({
	"Transaction": { screen: Transaction },
	"Product's": { screen: Products }
});

export default class index extends Component {
  render() {
    return (
      <MainNavigator />
    );
  }
}
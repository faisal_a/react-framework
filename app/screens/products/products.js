import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';

import addproduct from './tab/addproduct';
import productlist from './tab/productslist_nav';

const TabNavigator = createMaterialTopTabNavigator({
	"All Product": { screen: productlist },
	"Add Product": { screen: addproduct }
},
{
	lazy: true,
	tabBarOptions: {
	  activeTintColor:'#000000',
	  inactiveTintColor:'#CFCFCF',
	  labelStyle: {
	    fontSize: 12,
	    color: "#FFFFFF",
	  },
	  // tabStyle: {
	  //   width: 100,
	  // },
	  style: {
	    backgroundColor: '#4286f4',
	  },
	  indicatorStyle: {
        backgroundColor: '#FFFFFF',
       },
	}
});

export default class products extends Component {
  render() {
    return (
      <TabNavigator />
    )
  }
};
import React, { Component } from 'react';
import { View, Text, Button, TextInput, Alert } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';

import ProductStyle from '../../../assets/style/style';
import { api_product } from '../../../config/urls';


export default class addproduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nama_produk: null,
      harga_produk: null,
    };
  }


  addProductToFb = async () => {
    
    if(this.state.nama_produk === null || this.state.nama_produk === '') {
      Alert.alert("Perhatian", "Nama produk tidak boleh kosong");
    }else if(this.state.harga_produk === null || this.state.harga_produk === ''){
      Alert.alert("Perhatian", "Harga Produk tidak boleh kosong");

    }else if(isNaN(this.state.harga_produk)){
      Alert.alert("Perhatian", "Harga Produk harus nomor");
    }else{
        fetch('https://orderapp-3a853.firebaseio.com/product.json?auth=zAHW4PqI2Vvw3hp9C86eVxasJZRHBSa3vBwYqknA', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            nama_produk: this.state.nama_produk,
            harga_produk: this.state.harga_produk,
          }),
        })
        .then((response) => response.json())
          .then((responseJson) => {
          Alert.alert("Berhasil", "data masuk ke firebase");
          })
          .catch((error) => {
            console.error(error);
          });
      }
    }



  render() {
    return (
    <View>
	    <Text style={ProductStyle.productTitle}>Add Product</Text>
	   	<TextInput
          style={ProductStyle.inputStyle}
          placeholder="Nama Produk"
          onChangeText={(nama_produk) => this.setState({nama_produk})}
        />
        <TextInput
          style={ProductStyle.inputStyleBottom}
          placeholder="Harga Produk"
          onChangeText={(harga_produk) => this.setState({harga_produk})}
        />
        <Button
          onPress={this.addProductToFb}
          title="Submit"
          color="#FFD626"
          style={ProductStyle.submitBtn01}
          accessibilityLabel="Learn more about this purple button"
        />
	</View>
    )
  }
};
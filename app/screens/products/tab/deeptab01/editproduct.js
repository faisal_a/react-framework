import React, { Component } from 'react';
import { View, Text, Button, TextInput, Alert, Image, CameraRoll, ScrollView } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';

import ProductStyle from '../../../../assets/style/style';

export default class editproduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nama_produk: null,
      harga_produk: null,
      data: [],
      image_source: require('../../../../assets/img/tricker.jpg'),
    };
  }

  static navigationOptions = {
    title: 'Edit Product',
  };

  load(){
    const { params } = this.props.navigation.state;

    return fetch('https://orderapp-3a853.firebaseio.com/product/'+params.name+'.json?auth=zAHW4PqI2Vvw3hp9C86eVxasJZRHBSa3vBwYqknA')
    .then((response) => response.json())
    .then((responseJson) => {

      this.setState({
        isLoading: false,
        data : Object.values(responseJson),
        nama_produk: responseJson.nama_produk,
        harga_produk: responseJson.harga_produk,
        id : params.name,
      }, function(){

      });

      console.log(this.state.data);
      console.log(params.name);
      console.log(this.state.image_source);

    })
    .catch((error) =>{
      console.error(error);
    });
  }


  addProductToFb = async () => {
    
    const { params } = this.props.navigation.state;

    if(this.state.nama_produk === null || this.state.nama_produk === '') {
      Alert.alert("Perhatian", "Nama produk tidak boleh kosong");
    }else if(this.state.harga_produk === null || this.state.harga_produk === ''){
      Alert.alert("Perhatian", "Harga Produk tidak boleh kosong");

    }else if(isNaN(this.state.harga_produk)){
      Alert.alert("Perhatian", "Harga Produk harus nomor");
    }else{
        fetch('https://orderapp-3a853.firebaseio.com/product/'+params.name+'.json?auth=zAHW4PqI2Vvw3hp9C86eVxasJZRHBSa3vBwYqknA', {
          method: 'PATCH',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            nama_produk: this.state.nama_produk,
            harga_produk: this.state.harga_produk,
          }),
        })
        .then((response) => response.json())
          .then((responseJson) => {
          Alert.alert("Berhasil", "data masuk ke firebase");
          })
          .catch((error) => {
            console.error(error);
          });
      }

      this.load();
    }
  
    _handleButtonPress = () => {

      const options = {
        title: 'Select Image',
        customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      };

      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
      
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      
          this.setState({
            image_source: source,
          });
        }
        console.log(this.state.image_source);
      });
    };
    
  componentDidMount(){
    this.load();
  }
  
  render() {

    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };

    return (
      <View style={ProductStyle.container}>
        <Text style={ProductStyle.productTitle}>Edit Product {this.state.data[0]}</Text>
        <View style={ProductStyle.formStyle}>
          <Image
            style={{width: 100, height: 100}}
            source={this.state.image_source}
          />
          <TextInput
              style={ProductStyle.inputStyle}
              placeholder="Nama Produk"
              onChangeText={(nama_produk) => this.setState({nama_produk})}
              value={this.state.nama_produk}
            />
            <TextInput
              style={ProductStyle.inputStyleBottom}
              placeholder="Harga Produk"
              onChangeText={(harga_produk) => this.setState({harga_produk})}
              value={this.state.harga_produk}
            />
            <Button
              onPress={this.addProductToFb}
              title="Update"
              color="#4286f4"
              accessibilityLabel="Learn more about this"
            />
            <Button title="Load Images" onPress={this._handleButtonPress} />
           
        </View>
        
      </View>
    )
  }
};
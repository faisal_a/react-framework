import React, { Component } from 'react';
import { View, Text, FlatList, ListView, ActivityIndicator, Alert, TouchableWithoutFeedback } from 'react-native';
import { Button, List, ListItem } from 'react-native-elements'
import { createStackNavigator } from 'react-navigation';

import editproduct from './editproduct';
import selfscreen from './productslist';

export default class products_list extends Component {

constructor(props){
    super(props);
    this.state ={ 
      isLoading: true,
      refresh: true,
    }
  }

  load(){
    return fetch('https://orderapp-3a853.firebaseio.com/product.json?auth=zAHW4PqI2Vvw3hp9C86eVxasJZRHBSa3vBwYqknA')
    .then((response) => response.json())
    .then((responseJson) => {

      var data_state = [];

      if(responseJson != null){
        const data_init_key = Object.keys(responseJson);
        const data_init = Object.values(responseJson);

        for (let index = 0; index < data_init.length; index++) {
          data_state.push({
            'id' : data_init_key[index],
            'key' : index,
            'produk' : data_init[index]['nama_produk'],
            'harga' : data_init[index]['harga_produk'],
          });
          
        }
      }
      console.log(data_state);
      this.setState({
        isLoading: false,
        data : data_state
      }, function(){

      });

    })
    .catch((error) =>{
      console.error(error);
    });

    
  }

  componentDidMount(){
    this._interval = setInterval(() => {
      this.load();
    }, 5000);
    
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }


  render(){

    const { navigate } = this.props.navigation;

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(

      <View>
          <List>
              <FlatList
                  data={this.state.data}
                  renderItem={({item}) => 
                  <TouchableWithoutFeedback onPress={() => navigate('Edit Product', { name: item.id })}>
                    <ListItem
                      title={item.produk}
                      subtitle={item.harga}
                    />
                  </TouchableWithoutFeedback>
                  }
                  keyExtractor={key => key.toString()}
                />
          </List>
      </View>

    );
  }
}
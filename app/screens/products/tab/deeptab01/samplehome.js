import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default class samplehome extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <Text>This is the Sample Home screen</Text>
        <Button
            title="Go to Edit Product"
            onPress={() =>
                navigate('Edit Product', { name: 'Moherfucker' })
            }
        />
      </View>
    )
  }
};
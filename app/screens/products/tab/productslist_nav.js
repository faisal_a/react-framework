import React, { Component } from 'react';
import { View, Text, Button, FlatList, ListView, ActivityIndicator } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import editproduct from './deeptab01/editproduct';
import productlist from './deeptab01/productslist';
import samplehome from './deeptab01/samplehome';

const DeepNavigator = createStackNavigator({
	"All Product": { screen: productlist },
	"Edit Product": { screen: editproduct }
},{
  navigationOptions: {
    headerTransparent: true,
  },
});

export default class productslist_nav extends Component {
  render() {
    return (
      <DeepNavigator />
    )
  }
}
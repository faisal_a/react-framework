/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import index from './app/screens/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => index);
